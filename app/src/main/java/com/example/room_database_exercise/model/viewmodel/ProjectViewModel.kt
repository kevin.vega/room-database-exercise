package com.example.room_database_exercise.model.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.room_database_exercise.data.ProjectDatabase
import com.example.room_database_exercise.repository.ProjectRepository
import com.example.room_database_exercise.model.Project
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProjectViewModel(application: Application): AndroidViewModel(application) {
    var readAllData: LiveData<List<Project>>
    private val repository: ProjectRepository

    init {
        val projectDao = ProjectDatabase.getDatabase(application).projectDao()
        repository = ProjectRepository(projectDao)
        readAllData = repository.getProjects()
    }

     fun addProject(project: Project) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addProject(project)
        }
    }

    fun updateProject(project: Project) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateProject(project)
        }
    }

    fun deleteProject(project: Project) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteProject(project)
        }
    }

    fun deleteAllProjects() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAllProjects()
        }
    }
}

// Reference: Stevdza-San -  https://www.youtube.com/watch?v=orH4K6qBzvE